package pizzashop;
import java.util.ArrayList;
import java.util.List;


/** A customer order containing food items.
 *  The iterator provides an iterator over the FoodItems in the order.
 *  The Observable notifies observers whenever the order changes.
 *  The FoodOrder itself is attached as an Observer of FoodItems so
 *  it knows when a FoodItem changes.   
 */
public class FoodOrder {
	/** list of items in the order */
	private List<OrderItem> items;
	
	/** create a new, empty food order */
	public FoodOrder() {
		items = new ArrayList<OrderItem>();
	}
	/** add an item to the order.
	 *  @param item is the fooditem to add to order	
	 *  @return true if the item is successfully added
	 */
	public boolean addOrderItem(OrderItem item) {
		items.add( item );
		return true;
	}
	/** get the total price of the order
	 *  @return total price
	 */
	public double getTotal() {
		double price = 0;
		for( OrderItem i : items ) {
			price += i.getPrice();
		}
		return price;
	}
	public void printOrder() {
		for(OrderItem p: items) System.out.println( p.toString() );
		System.out.println("Total price: " + getTotal() );
	}


}