package pizzashop;

public interface OrderItem {
	public String toString();
	public double getPrice();
	public Object clone();
}
