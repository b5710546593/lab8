package pizzashop;

public abstract class AbstractItem {
	private int size;
	public AbstractItem(int size){
		this.size = size;
	}
	public void setSize(int size) { this.size = size; }
	public int getSize(){ return size; }
}
